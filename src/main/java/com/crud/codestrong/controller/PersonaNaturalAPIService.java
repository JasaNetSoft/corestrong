package com.crud.codestrong.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crud.codestrong.model.PersonaNaturalModel;

@RestController
@RequestMapping("/cloudusuariofinanciero")
public class PersonaNaturalAPIService {
	
	PersonaNaturalModel cloudUsuarioFinancierio;
	String Id;
	
	@GetMapping("{Id}")
	public PersonaNaturalModel getCloudUsuarioFinancieroDetails(String Id)
	{
		return cloudUsuarioFinancierio;
	}
	
	@PostMapping
	public String createCloudUsuarioFinancierioDetails(@RequestBody PersonaNaturalModel cloudUsuarioFinancierio )
	{
		this.cloudUsuarioFinancierio = cloudUsuarioFinancierio;
		return "Usuario Financierio Creado Satisfactoriamente";
	}
	
	@PutMapping
	public String updateCloudUsuarioFinancierioDetails(@RequestBody PersonaNaturalModel cloudUsuarioFinancierio )
	{
		this.cloudUsuarioFinancierio = cloudUsuarioFinancierio;
		return "Usuario Financierio Actualizado Satisfactoriamente";
	}
	
	@DeleteMapping("{Id}")
	public String deleteCloudUsuarioFinancierioDetails(String Id)
	{
		this.Id = null;
		return "Usuario Financierio Eliminado Satisfactoriamente";
	}
	
}
