package com.crud.codestrong.service.impl;

import com.crud.codestrong.model.PersonaNaturalModel;
import com.crud.codestrong.repository.CloudPersonaNaturalRepository;
import com.crud.codestrong.service.CloudPerNaturalService;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class CloudPerNaturalServiceImpl implements CloudPerNaturalService{

		CloudPersonaNaturalRepository PersonaNaturalRepository;

	    public CloudPerNaturalServiceImpl(CloudPersonaNaturalRepository PersonaNaturalRepository) {
	        this.PersonaNaturalRepository = PersonaNaturalRepository;
	    }

	    @Override
	    public String createPersonaNatural(PersonaNaturalModel naturalModel) {
	    	PersonaNaturalRepository.save(naturalModel);
	        return "Success";
	    }

	    @Override
	    public String updatePersonaNatural(PersonaNaturalModel naturalModel) {
	    	PersonaNaturalRepository.save(naturalModel);
	        return "Success";
	    }

	    @Override
	    public String deletePersonaNatural(String Id) {
	    	PersonaNaturalRepository.findByUsuarioName(Id);
	        return "Success";
	    }

	    @Override
	    public PersonaNaturalModel getPersonaNatural(String Id) {
	        if(PersonaNaturalRepository.findById(Id).isEmpty())
	        return PersonaNaturalRepository.findById(Id).get();
	    }

	    @Override
	    public List<PersonaNaturalModel> getAllPersonNatural() {
	        return PersonaNaturalRepository.findAll();
	    }

	    @Override
	    public List<PersonaNaturalModel> getByPersonaNaturalName(String vendorName)
	    {
	        return PersonaNaturalRepository.findByUsuarioName(vendorName);
	    }
}
