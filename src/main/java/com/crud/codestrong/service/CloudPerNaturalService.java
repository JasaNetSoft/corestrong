package com.crud.codestrong.service;

import com.crud.codestrong.model.PersonaNaturalModel;

import java.util.List;

public interface CloudPerNaturalService {
    public String createPersonaNatural(PersonaNaturalModel PersonaNatural);
    public String updatePersonaNatural(PersonaNaturalModel PersonaNatural);
    public String deletePersonaNatural(String PersonaNaturalId);
    public PersonaNaturalModel getPersonaNatural(String PersonaNaturalId);
    public List<PersonaNaturalModel> getAllPersonNatural();
    public List<PersonaNaturalModel> getByPersonaNaturalName(String vendorName);
}