package com.crud.codestrong.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.Id;

@Entity
@Table(name="usuario_financierio")
public class PersonaNaturalModel 
{
	@Id
	
	private String Id;
	private String DateCreate;
	private String UserCreate;
	private String DateModify;
	private String UserModify;
	
	public PersonaNaturalModel() {
	}

	public PersonaNaturalModel(String id, String dateCreate, String userCreate, String dateModify, String userModify) {
		Id = id;
		DateCreate = dateCreate;
		UserCreate = userCreate;
		DateModify = dateModify;
		UserModify = userModify;
	}
	
	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getDateCreate() {
		return DateCreate;
	}

	public void setDateCreate(String dateCreate) {
		DateCreate = dateCreate;
	}

	public String getUserCreate() {
		return UserCreate;
	}

	public void setUserCreate(String userCreate) {
		UserCreate = userCreate;
	}

	public String getDateModify() {
		return DateModify;
	}

	public void setDateModify(String dateModify) {
		DateModify = dateModify;
	}

	public String getUserModify() {
		return UserModify;
	}

	public void setUserModify(String userModify) {
		UserModify = userModify;
	}



	
}
