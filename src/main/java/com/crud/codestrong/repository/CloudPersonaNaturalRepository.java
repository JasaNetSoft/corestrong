package com.crud.codestrong.repository;

import com.crud.codestrong.model.PersonaNaturalModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CloudPersonaNaturalRepository extends JpaRepository<PersonaNaturalModel, String>
{
    List<PersonaNaturalModel> findByUsuarioName(String usuarioNombre);
}


